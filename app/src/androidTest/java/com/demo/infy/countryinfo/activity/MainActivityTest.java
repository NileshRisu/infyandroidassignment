package com.demo.infy.countryinfo.activity;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.demo.infy.countryinfo.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Nilesh-PC on 04/07/2018.
 */
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    public MainActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch() {
        View view = mActivity.findViewById(R.id.activity_main_coordinatorlayout);
        View vTestTitle=mActivity.findViewById(R.id.txt_screen_title);
        View viewLytContainer=mActivity.findViewById(R.id.fragment_container);
        assertNotNull(view);
        assertNotNull(vTestTitle);
        assertNotNull(viewLytContainer);

    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

}