package com.demo.infy.countryinfo.fragment;

import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.RelativeLayout;

import com.demo.infy.countryinfo.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nilesh-PC on 04/07/2018.
 */
public class CountryInfoFragmentTest {
    @Rule
    public ActivityTestRule<TestActivity> mActivityTestRule = new ActivityTestRule<TestActivity>(TestActivity.class);
    private TestActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void onCreateView() throws Exception {
        RelativeLayout rlContainer = mActivity.findViewById(R.id.test_container);
        assertNotNull(rlContainer);
        CountryInfoFragment fragment = new CountryInfoFragment();

        mActivity.getSupportFragmentManager().beginTransaction().add(rlContainer.getId(), fragment).commitAllowingStateLoss();
        getInstrumentation().waitForIdleSync();
        View view = fragment.getView().findViewById(R.id.recyclerView_CountryInfo);
        assertNotNull(view);

    }

    @Test
    public void countriesReady() throws Exception {
    }

    @Test
    public void onRefresh() throws Exception {
    }

}