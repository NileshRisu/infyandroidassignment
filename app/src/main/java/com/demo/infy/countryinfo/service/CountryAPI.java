package com.demo.infy.countryinfo.service;



import com.demo.infy.countryinfo.module.ModelCountryInfo;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * This class represents the Countries API, all endpoints can stay here.
 *
 * Created by Nilesh on 1-July-18.
 */
public interface CountryAPI {

    @GET("/s/2iodh4vg0eortkl/facts.jsonmight")
    Call<ModelCountryInfo> getResults();
}
