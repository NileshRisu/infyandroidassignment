package com.demo.infy.countryinfo.service;


import com.demo.infy.countryinfo.util.WebService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * This class represents the country service.
 *
 Created by Nilesh on 1-July-18.
 */
public class CountryService {
    private Retrofit retrofit = null;


    /**
     * This method creates a new instance of the API interface.
     *
     * @return The API interface
     */
    public CountryAPI getAPI() {
       // String BASE_URL = "http://services.groupkt.com/";
        String BASE_URL = WebService.MAIN_URL;

        if (retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit.create(CountryAPI.class);
    }
}
