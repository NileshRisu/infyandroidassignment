package com.demo.infy.countryinfo.presenter;



import com.demo.infy.countryinfo.module.ModelCountryInfo;
import com.demo.infy.countryinfo.module.Row;
import com.demo.infy.countryinfo.service.CountryService;
import com.demo.infy.countryinfo.view.CountryView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class represents the country  com.demo.infy.countryinfo.view interface.
 *
 * Created by Nilesh on 1-July-18.
 */
public class CountryPresenter {


    private CountryView countryView;
    private CountryService countryService;

    public CountryPresenter(CountryView view) {
        this.countryView = view;

        if (this.countryService == null) {
            this.countryService = new CountryService();
        }
    }

    public void getCountries() {
        countryService
                .getAPI()
                .getResults()
                .enqueue(new Callback<ModelCountryInfo>() {
                    @Override
                    public void onResponse(Call<ModelCountryInfo> call, Response<ModelCountryInfo> response) {
                        ModelCountryInfo data = response.body();
                        //set title
                       // MainActivity.txt_screen_title.setText(data.getTitle());
                        countryView.updateTitle(data.getTitle());
                        if (data.getRows() != null) {
                            List<Row> result = data.getRows();
                            countryView.countriesReady(result);
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelCountryInfo> call, Throwable t) {
                        try {
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

}
