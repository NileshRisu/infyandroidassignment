package com.demo.infy.countryinfo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.infy.countryinfo.R;
import com.demo.infy.countryinfo.activity.MainActivity;
import com.demo.infy.countryinfo.adapter.CountryInfoAdapter;
import com.demo.infy.countryinfo.module.Row;
import com.demo.infy.countryinfo.presenter.CountryPresenter;
import com.demo.infy.countryinfo.view.CountryView;

import java.util.List;


/*
 * Created by Nilesh on 1-July-18.
 */

public class CountryInfoFragment extends android.support.v4.app.Fragment implements CountryView, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recyclerView_CountryInfo;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /* Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.fragment_countryinfo, null);
        //------------------------------Initialize-----------------------
        init(v);
        //---------------------fetch country info---------------------------------
        final CountryPresenter countryPresenter = new CountryPresenter(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        // Maybe it's best to call it on onResume()
                                        countryPresenter.getCountries();
                                    }
                                }
        );
        return v;
    }


    private void init(View view) {
        recyclerView_CountryInfo = view.findViewById(R.id.recyclerView_CountryInfo);
        recyclerView_CountryInfo.setHasFixedSize(true);
        recyclerView_CountryInfo.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
    }

    @Override
    public void countriesReady(List<Row> countries) {
        // See your Logcat :)
        for (Row country : countries) {
            Log.i("RETROFIT", country.getTitle() + "\n"
                    + " - Alpha2:  " + country.getDescription() + " \n"
                    + " - Alpha3: " + country.getImageHref());
        }
        //creating recyclerview adapter
        CountryInfoAdapter adapter = new CountryInfoAdapter(getActivity(), countries);

        //setting adapter to recyclerview
        recyclerView_CountryInfo.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateTitle(String title) {
        ((MainActivity) getActivity()).updateUserInfoTextView(title);

    }

    @Override
    public void onRefresh() {
        //-------------------refresh data----------------------
        //---------------------fetch country info---------------------------------
        //  fetchCountryInfo();
        CountryPresenter countryPresenter = new CountryPresenter(this);

        // Maybe it's best to call it on onResume()
        countryPresenter.getCountries();

    }
}
