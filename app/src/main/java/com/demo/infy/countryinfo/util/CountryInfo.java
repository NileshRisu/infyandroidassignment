package com.demo.infy.countryinfo.util;


import com.demo.infy.countryinfo.module.ModelCountryInfo;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Nilesh on 1-July-18.
 */

public interface CountryInfo {
    String path=WebService.SUB_URL;
    @GET(path)
    Call<ModelCountryInfo> fetch();
}
