package com.demo.infy.countryinfo.view;

import com.demo.infy.countryinfo.module.Row;

import java.util.List;

/**
 * This class represents the country countryinfoapp.demoapp.com.countryinfoapp.view interface.
 *
 * Created by Nilesh on 1-July-18.
 */
public interface CountryView {

    void countriesReady(List<Row> countries);

void updateTitle(String title);
}
