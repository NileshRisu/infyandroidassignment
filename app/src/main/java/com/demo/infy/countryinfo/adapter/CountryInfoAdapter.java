package com.demo.infy.countryinfo.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.infy.countryinfo.R;
import com.demo.infy.countryinfo.module.Row;
import com.squareup.picasso.Picasso;

import java.util.List;


/*
 * Created by Nilesh on 1/07/2018.
 */
public class CountryInfoAdapter extends RecyclerView.Adapter<CountryInfoAdapter.ProductViewHolder> {
    //this context we will use to inflate the layout
    private Context mCtx;
    //we are storing all the products in a list
    private List<Row> countryList;
    //getting the context and product list with constructor
    public CountryInfoAdapter(Context mCtx, List<Row> countryList) {
        this.mCtx = mCtx;
        this.countryList = countryList;
    }

    @Override
    @NonNull
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent,int viewType) {
        //inflating and returning our view holder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country, parent, false);
        return new ProductViewHolder(view);

    }

    @Override

    public void onBindViewHolder(@NonNull  ProductViewHolder holder, int position) {
        //getting the product of the specified position
        Row row_val = countryList.get(position);

        //binding the data with the viewholder views
        holder.text_Title.setText(row_val.getTitle());
        holder.text_description.setText(row_val.getDescription());

        if(row_val.getImageHref()!=null) {
            Picasso.with(mCtx).load(row_val.getImageHref().toString()).into(holder.img_row);

        }

    }


    @Override
    public int getItemCount() {
        return countryList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView text_description, text_Title;
        ImageView img_row;

        private ProductViewHolder(View itemView) {
            super(itemView);

            text_description = itemView.findViewById(R.id.text_description);
            text_Title = itemView.findViewById(R.id.text_Title);

            img_row = itemView.findViewById(R.id.img_row);
        }
    }
}
